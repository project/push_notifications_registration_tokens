# Push Notifications Registration Tokens

This module provides an entity type for registering push notification tokens.

The module does not send push notifications; to do that, you will need to use this module in combination with the [Firebase PHP module](https://www.drupal.org/project/firebase_php).

* Protects user privacy by default (no additional device info logged).
* Automatically deletes stale tokens after 90 days in accordance with Google and Apple recommendations (this value can be changed, and auto-deletion can be disabled).
* When adding a token, you can specify the type: `android`, `apple`, or `web`.
* If you install the [Universal Device Detection module](https://www.drupal.org/project/universal_device_detection) and enable the config setting, you can log the device user agent when adding a token.
* Provides a JSON-RPC endpoint for saving tokens. (REST endpoint not yet available; please contribute!)

For a full description of the module, visit the
[project page](https://www.drupal.org/project/push_notifications_registration_tokens).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/push_notifications_registration_tokens).


## Table of contents

- Requirements
- Recommended modules
- Installation
- Module Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Recommended modules

* [Firebase PHP module](https://www.drupal.org/project/firebase_php): Use this module to send push notifications to the tokens registered with this module.
* [Universal Device Detection module](https://www.drupal.org/project/universal_device_detection): Install if you want to log the device user agent string.


## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Module Configuration

1. Enable the module at Administration > Extend.
1. Go to `/admin/config/system/push_notifications_registration_tokens` to configure the module.
1. By default, this module deletes stale tokens after 90 days. Set this to whatever value you like (`0` to disable).
1. By default, this module does not log the user agent string when saving a token. If you want to log device info, install the Universal Device Detection module and enable the relevant option.


### JSON-RPC Token Save Endpoint Configuration

1. `drush pm-enable jsonrpc_add_registration_token -y`
1. Give authenticated users  the `Register tokens by JSON-RPC` permission.
1. In your frontend app, call the JSON-RPC endpoint as follows:

```
{
    "jsonrpc": "2.0",
    "method": "user.add_push_token",
    "id": "push-token-my-uuid-or-whatever-you-want",
    "params": {
        "token": "a-really-long-valid-push-token",
        "tokenType": "apple"
    }
}
```

`tokenType` can be `apple`, `android`, or `web`. It is not required.


## Maintainers (optional)

- Patrick Kenny - [ptmkenny](https://www.drupal.org/u/ptmkenny)
