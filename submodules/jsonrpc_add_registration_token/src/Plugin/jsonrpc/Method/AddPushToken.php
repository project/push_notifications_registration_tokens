<?php

declare(strict_types=1);

namespace Drupal\jsonrpc_add_registration_token\Plugin\jsonrpc\Method;

use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\jsonrpc\Exception\JsonRpcException;
use Drupal\jsonrpc\MethodInterface;
use Drupal\jsonrpc\Object\Error;
use Drupal\jsonrpc\Object\ParameterBag;
use Drupal\jsonrpc\Plugin\JsonRpcMethodBase;
use Drupal\push_notifications_registration_tokens\Enum\PushNotificationTokenType;
use Drupal\push_notifications_registration_tokens\ProcessTokensInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Webmozart\Assert\Assert;

/**
 * Adds a push token to the user's account.
 *
 * @JsonRpcMethod(
 *   id = "user.add_push_token",
 *   usage = @Translation("Registers a push token to the user's account."),
 *   access = {"register tokens by jsonrpc"},
 *   params = {
 *     "token" = @JsonRpcParameterDefinition(
 *       schema={"type"="string"},
 *       required=true
 *     ),
 *     "tokenType" = @JsonRpcParameterDefinition(
 *       schema={"type"="string"},
 *       required=false
 *     ),
 *   }
 * ),
 */
final class AddPushToken extends JsonRpcMethodBase {
  use LoggerChannelTrait;

  public function __construct(
    array $configuration,
    string $plugin_id,
    MethodInterface $plugin_definition,
    protected ProcessTokensInterface $processTokens,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): AddPushToken {
    return new self(
      $configuration, $plugin_id, $plugin_definition,
      $container->get('push_notifications_registration_tokens.process_tokens'),
    );
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function execute(ParameterBag $params): bool {
    try {
      $push_token = $params->get('token');
      Assert::stringNotEmpty($push_token);
      $token_type = $params->get('tokenType');
      if (is_string($token_type)) {
        $validated_token_type = PushNotificationTokenType::from($token_type);
      }

      return $this->processTokens->registerOrRefreshToken($push_token, $validated_token_type ?? NULL);
    }
    /* @phpstan-ignore-next-line Thrown exceptions in catch block must bundle previous exception. */
    catch (\LogicException $e) {
      $error_message = $e->getMessage();
      $this->getLogger('jsonrpc')->error($error_message);
      $error = Error::internalError();
      throw JsonRpcException::fromError($error);
    }
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function outputSchema(): array {
    return ['type' => 'boolean'];
  }

}
