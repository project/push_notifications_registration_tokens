<?php

namespace Drupal\Tests\jsonrpc_add_registration_token\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test installation and uninstallation.
 *
 * @group jsonrpc_add_registration_token
 */
class JsonrpcAddRegistrationTokenInstallTest extends KernelTestBase {

  private const string MODULE_NAME = 'jsonrpc_add_registration_token';

  /**
   * Test that the module can be installed and uninstalled.
   */
  public function testInstallUninstall(): void {
    \Drupal::service('module_installer')->install([self::MODULE_NAME]);

    \Drupal::service('module_installer')->uninstall([self::MODULE_NAME]);

    // Try installing and uninstalling again.
    \Drupal::service('module_installer')->install([self::MODULE_NAME]);

    \Drupal::service('module_installer')->uninstall([self::MODULE_NAME]);
  }

}
