<?php

declare(strict_types=1);

namespace Drupal\firebase_php_integration\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Utility\Error;
use Drupal\firebase_php\Exception\FirebasePhpInvalidArgumentException;
use Drupal\firebase_php\FirebasePhpMessagingApiInterface;
use Drupal\push_notifications_registration_tokens\ProcessTokensInterface;
use Drupal\push_notifications_registration_tokens\RegistrationTokenInterface;
use Drupal\user\UserInterface;
use Drupal\user\UserStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to send a test push notification.
 */
final class FirebasePhpIntegrationSendMessageSingleUserForm extends FormBase {

  public function __construct(
    protected FirebasePhpMessagingApiInterface $messagingService,
    protected UserStorageInterface $userStorage,
    protected ProcessTokensInterface $processTokens,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function create(ContainerInterface $container): FirebasePhpIntegrationSendMessageSingleUserForm {
    return new self(
      $container->get('firebase_php.messaging_drupal_api'),
      $container->get('entity_type.manager')->getStorage('user'),
      $container->get('push_notifications_registration_tokens.process_tokens')
    );
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getFormId(): string {
    return 'firebase_php_integration_send_message_single_user_form';
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $this->t('Exciting test message! 🥳'),
      '#maxlength' => 255,
      '#required' => TRUE,
    ];
    $form['message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Message'),
      '#default_value' => $this->t('This is a test message. It includes an emoji. 😄'),
      '#maxlength' => 255,
      '#required' => TRUE,
    ];
    $form['badge_count'] = [
      '#type' => 'number',
      '#title' => $this->t('Badge count'),
      '#required' => FALSE,
    ];

    $form['user'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'user',
      '#title' => $this->t('User to send a message to'),
      '#description' => $this->t('Choose a user that has push tokens registered with the Push Notifications Registration Tokens module.'),
      '#required' => TRUE,
    ];
    $form['most_recent_token_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send only to the most recent token'),
      '#description' => $this->t("Sends the message only to the user's most recently used device token. If unchecked, sends the message to all of the user's registered device tokens."),
      '#default_value' => TRUE,
      '#required' => FALSE,
    ];

    // Group submit handlers in an actions element with a key of "actions" so
    // that it gets styled correctly, and so that other modules may add actions
    // to the form. This is not required, but is convention.
    $form['actions'] = [
      '#type' => 'actions',
    ];

    // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    try {
      $user_value = $form_state->getValue('user');
      $user_account = $this->userStorage->load($user_value);
      if (!($user_account instanceof UserInterface)) {
        throw new FirebasePhpInvalidArgumentException("Failed to load user to send token! $user_value");
      }

      $send_only_to_most_recent_token = $form_state->getValue('most_recent_token_only');
      if ($send_only_to_most_recent_token == TRUE) {
        $token = $this->processTokens->getMostRecentTokenForUser($user_account);
        if (!($token instanceof RegistrationTokenInterface)) {
          $this->messenger()->addWarning($this->t('The specified user has no push tokens registered.'));
          return;
        }
        $single_device_result = $this->messagingService->sendMessageSingleDevice(
          $form_state->getValue('title'),
          $form_state->getValue('message'),
          $token->getToken(),
          NULL,
          NULL,
          NULL,
          intval($form_state->getValue('badge_count')),
        );
        $this->messenger()->addStatus($this->t('The test message has been sent. Result: %results',
        ['%result' => json_encode($single_device_result)]));
      }
      else {
        $tokens = $this->processTokens->getAllTokensForUser($user_account);
        if ($tokens === []) {
          $this->messenger()->addWarning($this->t('The specified user has no push tokens registered.'));
          return;
        }
        $multiple_device_results = $this->messagingService->sendMessageMultipleDevices(
          $tokens,
          $form_state->getValue('title'),
          $form_state->getValue('message'),
          NULL,
          NULL,
          NULL,
          intval($form_state->getValue('badge_count')),
              );
        $this->messenger()->addStatus($this->t('The test messages have been sent. Results: %results',
              ['%results' => json_encode($multiple_device_results)]));
      }

    }
    // @phpstan-ignore-next-line Exception is shown to admin, so no rethrowing.
    catch (\Exception $e) {
      Error::logException($this->logger('firebase_php'), $e);
      $details = method_exists($e, 'errors') ? $e->errors() : 'No details available.';
      $this->messenger()->addError($this->t('Failed to send the test message. Error: %error More info: %errorDetails', [
        '%error' => $e->getMessage(),
        '%errorDetails' => json_encode($details),
      ]));
    }
  }

}
