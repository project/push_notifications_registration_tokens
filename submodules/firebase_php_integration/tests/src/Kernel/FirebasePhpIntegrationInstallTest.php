<?php

namespace Drupal\Tests\firebase_php_integration\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test installation and uninstallation.
 *
 * @group pn_registration_tokens_integration
 */
class FirebasePhpIntegrationInstallTest extends KernelTestBase {

  private const string MODULE_NAME = 'firebase_php_integration';

  /**
   * Test that the module can be installed and uninstalled.
   */
  public function testInstallUninstall(): void {
    \Drupal::service('module_installer')->install([self::MODULE_NAME]);

    \Drupal::service('module_installer')->uninstall([self::MODULE_NAME]);

    // Try installing and uninstalling again.
    \Drupal::service('module_installer')->install([self::MODULE_NAME]);

    \Drupal::service('module_installer')->uninstall([self::MODULE_NAME]);
  }

}
