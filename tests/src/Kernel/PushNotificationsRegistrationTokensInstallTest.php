<?php

declare(strict_types=1);

namespace Drupal\Tests\push_notifications_registration_tokens\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test installation and uninstallation.
 *
 * @group push_notifications_registration_tokens
 */
class PushNotificationsRegistrationTokensInstallTest extends KernelTestBase {

  private const string MODULE_NAME = 'push_notifications_registration_tokens';

  /**
   * Test that the module can be installed and uninstalled.
   */
  public function testInstallUninstall(): void {
    \Drupal::service('module_installer')->install([self::MODULE_NAME]);

    \Drupal::service('module_installer')->uninstall([self::MODULE_NAME]);

    // Try installing and uninstalling again.
    \Drupal::service('module_installer')->install([self::MODULE_NAME]);

    \Drupal::service('module_installer')->uninstall([self::MODULE_NAME]);
  }

}
