<?php

declare(strict_types=1);

namespace Drupal\push_notifications_registration_tokens;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Provides a list controller for the registration token entity type.
 */
final class RegistrationTokenListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function buildHeader(): array {
    $header = [];

    $header['id'] = $this->t('ID');
    $header['uid'] = $this->t('Author');
    $header['changed'] = $this->t('Updated');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function buildRow(EntityInterface $entity): array {
    $row = [];

    /** @var \Drupal\push_notifications_registration_tokens\RegistrationTokenInterface $entity */
    $row['id'] = $entity->toLink();
    /** @var \Drupal\user\UserInterface $user_entity */
    $user_entity = $entity->get('uid')->entity;
    $username_options = [
      'label' => 'hidden',
      'settings' => ['link' => $user_entity->isAuthenticated()],
    ];
    $row['uid']['data'] = $entity->get('uid')->view($username_options);
    $row['changed']['data'] = $entity->get('changed')->view(['label' => 'hidden']);
    return $row + parent::buildRow($entity);
  }

}
