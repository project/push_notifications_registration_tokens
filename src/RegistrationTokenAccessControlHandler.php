<?php

declare(strict_types=1);

namespace Drupal\push_notifications_registration_tokens;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the registration token entity type.
 *
 * phpcs:disable Drupal.Arrays.Array.LongLineDeclaration
 *
 * @see https://www.drupal.org/project/coder/issues/3185082
 */
final class RegistrationTokenAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  #[\Override]
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResult {
    // Anonymous users should never have access to tokens.
    if ($account->isAnonymous()) {
      return AccessResult::forbidden();
    }

    $admin_permission = $this->entityType->getAdminPermission();
    if (is_string($admin_permission) && $account->hasPermission($admin_permission)) {
      return AccessResult::allowed()->cachePerPermissions();
    }

    return match($operation) {
      'view' => AccessResult::allowedIfHasPermission($account, 'view pn_registration_token'),
      'update' => AccessResult::allowedIfHasPermission($account, 'edit pn_registration_token'),
      'delete' => AccessResult::allowedIfHasPermission($account, 'delete pn_registration_token'),
      default => AccessResult::neutral(),
    };
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL): AccessResult {
    // Anonymous users cannot create tokens.
    // An exception will be thrown.
    if ($account->isAnonymous()) {
      return AccessResult::forbidden();
    }
    return AccessResult::allowedIfHasPermissions($account, ['create pn_registration_token', 'administer pn_registration_token'], 'OR');
  }

}
