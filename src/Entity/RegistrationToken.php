<?php

declare(strict_types=1);

namespace Drupal\push_notifications_registration_tokens\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\push_notifications_registration_tokens\Enum\PushNotificationTokenType;
use Drupal\push_notifications_registration_tokens\Enum\PushNotificationsRegistrationTokensSetting;
use Drupal\push_notifications_registration_tokens\Exception\PushNotificationsRegistrationTokensInvalidArgumentException;
use Drupal\push_notifications_registration_tokens\RegistrationTokenInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the registration token entity class.
 *
 * @ContentEntityType(
 *   id = "pn_registration_token",
 *   label = @Translation("Registration Token"),
 *   label_collection = @Translation("Registration Tokens"),
 *   label_singular = @Translation("registration token"),
 *   label_plural = @Translation("registration tokens"),
 *   label_count = @PluralTranslation(
 *     singular = "@count registration tokens",
 *     plural = "@count registration tokens",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\push_notifications_registration_tokens\RegistrationTokenListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\push_notifications_registration_tokens\RegistrationTokenAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\push_notifications_registration_tokens\Form\RegistrationTokenForm",
 *       "edit" = "Drupal\push_notifications_registration_tokens\Form\RegistrationTokenForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "pn_registration_token",
 *   admin_permission = "administer pn_registration_token",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/push-notification-registration-token",
 *     "add-form" = "/push-notification-registration-token/add",
 *     "canonical" = "/push-notification-registration-token/{pn_registration_token}",
 *     "edit-form" = "/push-notification-registration-token/{pn_registration_token}/edit",
 *     "delete-form" = "/push-notification-registration-token/{pn_registration_token}/delete",
 *     "delete-multiple-form" = "/admin/content/push-notification-registration-token/delete-multiple",
 *   },
 * )
 */
final class RegistrationToken extends ContentEntityBase implements RegistrationTokenInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * Sets the length of the field to use to store the token.
   *
   * It seems the token can be over 255 in some cases:
   * https://stackoverflow.com/a/40572076.
   */
  public const int PUSH_NOTIFICATION_TOKEN_MAX_LENGTH = 512;

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);
    $owner_id = $this->getOwnerId();
    if (!is_int($owner_id) && !is_string($owner_id)) {
      throw new PushNotificationsRegistrationTokensInvalidArgumentException("Owner ID for push notification token not set!");
    }
    elseif ($owner_id === 0 || $owner_id === '0') {
      throw new PushNotificationsRegistrationTokensInvalidArgumentException("Anonymous cannot be owner of push notification token!");
    }
    $module_handler = \Drupal::service('module_handler');
    if ($module_handler->moduleExists('universal_device_detection')) {
      $log_device_info_setting = \Drupal::config('push_notifications_registration_tokens.settings')->get(PushNotificationsRegistrationTokensSetting::LogDeviceInfo->value);
      if (boolval($log_device_info_setting)) {
        $detected_device = \Drupal::service('universal_device_detection.default')->detect();
        $this->set('device_info', json_encode($detected_device));
      }
    }
    // Manually set the changed time.
    // This is necessary because when refreshing the token,
    // no field values will change, so we have to force the time update.
    $this->setChangedTime(\Drupal::time()->getCurrentTime());
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(self::class . '::getDefaultEntityOwner')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['token'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Token'))
      ->setRequired(TRUE)
      ->setSetting('max_length', self::PUSH_NOTIFICATION_TOKEN_MAX_LENGTH)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->addConstraint('UniqueField');

    $fields['token_type'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Token Type'))
      ->setDescription(t('Specify the platform of the device that created the token.'))
      ->setRequired(FALSE)
      ->setSettings([
        'allowed_values' => [
          PushNotificationTokenType::Apple->value => 'Apple',
          PushNotificationTokenType::Android->value => 'Android',
          PushNotificationTokenType::Web->value => 'Web',
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'visible',
        'type' => 'list_default',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['device_info'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Device Info'))
      ->setRequired(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'text_default',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the registration token was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getToken(): string {
    return $this->get('token')->getString();
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getTokenType(): PushNotificationTokenType|null {
    $token_type_raw = $this->get('token_type')->getString();
    if ($token_type_raw === '') {
      return NULL;
    }
    return PushNotificationTokenType::from($token_type_raw);
  }

}
