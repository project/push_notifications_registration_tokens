<?php

declare(strict_types=1);

namespace Drupal\push_notifications_registration_tokens\Services;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\push_notifications_registration_tokens\Entity\RegistrationToken;
use Drupal\push_notifications_registration_tokens\Enum\PushNotificationTokenType;
use Drupal\push_notifications_registration_tokens\Enum\PushNotificationsRegistrationTokensSetting;
use Drupal\push_notifications_registration_tokens\Exception\PushNotificationsRegistrationTokensException;
use Drupal\push_notifications_registration_tokens\ProcessTokensInterface;
use Drupal\push_notifications_registration_tokens\RegistrationTokenInterface;
use Drupal\user\UserInterface;

/**
 * Registers, refreshes, and deletes push notification tokens.
 */
final class ProcessTokens implements ProcessTokensInterface {

  public function __construct(
    protected AccountInterface $currentUser,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected TimeInterface $time,
    protected ConfigFactoryInterface $configFactory,
  ) {}

  private const int SECONDS_IN_DAY = 86400;

  private const string REGISTRATION_TOKEN_ENTITY = 'pn_registration_token';

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function registerOrRefreshToken(string $token, ?PushNotificationTokenType $token_type): bool {
    $entity_storage = $this->entityTypeManager->getStorage(self::REGISTRATION_TOKEN_ENTITY);
    $existing_tokens = $entity_storage->getQuery()
      ->condition('token', $token)
      ->accessCheck(FALSE)
      ->execute();

    $existing_token_count = count($existing_tokens);

    // If the token is new, create an entity.
    if ($existing_token_count === 0) {
      $token_entity_values = [
        'uid' => $this->currentUser->id(),
        'token' => $token,
      ];
      if (isset($token_type)) {
        $token_entity_values['token_type'] = $token_type->value;
      }

      $registration_token = RegistrationToken::create($token_entity_values);
      $registration_token->save();
      return TRUE;
    }
    // If we already have a token, we need to refresh it.
    elseif ($existing_token_count === 1) {
      $token_id = reset($existing_tokens);
      $existing_token = $entity_storage->load($token_id);
      if ($existing_token instanceof RegistrationTokenInterface) {
        $existing_token->save();
        return FALSE;
      }
      throw new PushNotificationsRegistrationTokensException("Unable to get existing token with ID $existing_tokens[0]!");
    }
    else {
      throw new PushNotificationsRegistrationTokensException("Too many tokens with same value registered! $existing_token_count");
    }
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function deleteAllTokensForUser(UserInterface $user): void {
    $entity_storage = $this->entityTypeManager->getStorage(self::REGISTRATION_TOKEN_ENTITY);
    $all_user_tokens = $this->getAllTokensForUser($user);
    $entity_storage->delete($all_user_tokens);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function deleteAllStaleTokens(): void {
    $entity_storage = $this->entityTypeManager->getStorage(self::REGISTRATION_TOKEN_ENTITY);
    $delete_stale_tokens_setting = intval($this->configFactory->get('push_notifications_registration_tokens.settings')->get(PushNotificationsRegistrationTokensSetting::DeleteStaleTokens->value));
    if ($delete_stale_tokens_setting === 0) {
      // If 0, the site administrator does not want to delete tokens.
      return;
    }
    $current_time = $this->time->getCurrentTime();
    $time_to_delete_tokens = $current_time - ($delete_stale_tokens_setting * self::SECONDS_IN_DAY);
    $existing_tokens = $entity_storage->getQuery()
      ->condition('changed', $time_to_delete_tokens, '<=')
      ->accessCheck(FALSE)
      ->execute();
    $token_entities = $entity_storage->loadMultiple($existing_tokens);
    $entity_storage->delete($token_entities);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getAllTokensForUser(UserInterface $user): array {
    $entity_storage = $this->entityTypeManager->getStorage(self::REGISTRATION_TOKEN_ENTITY);
    $existing_tokens = $entity_storage->getQuery()
      ->condition('uid', $user->id())
      ->accessCheck(FALSE)
      ->execute();
    return $entity_storage->loadMultiple($existing_tokens);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getMostRecentTokenForUser(UserInterface $user): ?RegistrationTokenInterface {
    $entity_storage = $this->entityTypeManager->getStorage(self::REGISTRATION_TOKEN_ENTITY);
    $existing_tokens = $entity_storage->getQuery()
      ->condition('uid', $user->id())
      ->sort('changed', 'DESC')
      ->accessCheck(FALSE)
      ->execute();
    if (isset($existing_tokens[0])) {
      $most_recent_token = $entity_storage->load($existing_tokens[0]);
      /** @var \Drupal\push_notifications_registration_tokens\RegistrationTokenInterface $most_recent_token */
      return $most_recent_token;
    }
    return NULL;
  }

}
