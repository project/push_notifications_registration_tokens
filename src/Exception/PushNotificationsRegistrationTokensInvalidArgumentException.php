<?php

declare(strict_types=1);

namespace Drupal\push_notifications_registration_tokens\Exception;

/**
 * Invalid argument exception for Push Notifications Registration Tokens module.
 */
class PushNotificationsRegistrationTokensInvalidArgumentException extends PushNotificationsRegistrationTokensException {
}
