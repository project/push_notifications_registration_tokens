<?php

declare(strict_types=1);

namespace Drupal\push_notifications_registration_tokens\Exception;

/**
 * Exceptions for the Push Notifications Registration Tokens module.
 */
class PushNotificationsRegistrationTokensException extends \Exception {
}
