<?php

declare(strict_types=1);

namespace Drupal\push_notifications_registration_tokens;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\push_notifications_registration_tokens\Enum\PushNotificationTokenType;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a registration token entity type.
 */
interface RegistrationTokenInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the registration token.
   *
   * @return string
   *   The registration token.
   */
  public function getToken(): string;

  /**
   * Gets the token type.
   *
   * @return \Drupal\push_notifications_registration_tokens\Enum\PushNotificationTokenType|null
   *   The token type as an enum or null.
   */
  public function getTokenType(): PushNotificationTokenType|null;

}
