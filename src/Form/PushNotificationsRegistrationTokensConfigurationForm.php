<?php

declare(strict_types=1);

namespace Drupal\push_notifications_registration_tokens\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\push_notifications_registration_tokens\Enum\PushNotificationsRegistrationTokensSetting;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configures the Push Notifications Registration Tokens module.
 */
final class PushNotificationsRegistrationTokensConfigurationForm extends ConfigFormBase {

  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typed_config_manager,
    protected ModuleHandlerInterface $moduleHandler,
  ) {
    parent::__construct($config_factory, $typed_config_manager);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function create(ContainerInterface $container): PushNotificationsRegistrationTokensConfigurationForm {
    return new self(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('module_handler'),
    );
  }

  const string FORM_ID = 'push_notifications_registration_tokens.settings';

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getFormId(): string {
    return self::FORM_ID;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  protected function getEditableConfigNames(): array {
    return [self::FORM_ID];
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(self::FORM_ID);

    $form[self::FORM_ID] = [
      '#type' => 'details',
      '#title' => $this->t('Configure Push Notifications Registration Tokens'),
      '#open' => TRUE,
    ];

    $universal_device_detection_enabled = $this->moduleHandler->moduleExists('universal_device_detection');

    $form[self::FORM_ID][PushNotificationsRegistrationTokensSetting::DeleteStaleTokens->value] = [
      '#type' => 'number',
      '#title' => $this->t('Delete stale tokens after this many days'),
      '#description' => $this->t('Sending messages to devices that never open them is a waste of resources and lowers delivery rates. Set to 0 to disable.'),
      '#default_value' => $config->get(PushNotificationsRegistrationTokensSetting::DeleteStaleTokens->value),
      '#required' => TRUE,
    ];

    $form[self::FORM_ID][PushNotificationsRegistrationTokensSetting::LogDeviceInfo->value] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log device info when registering or refreshing a token'),
      '#description' => $universal_device_detection_enabled ?
      $this->t('Logs device info using the Universal Device Detection module, which is based on Matomo analytics.') :
      $this->t('To use this feature, enable the Universal Device Detection module.'),
      '#default_value' => $config->get(PushNotificationsRegistrationTokensSetting::LogDeviceInfo->value),
      '#required' => FALSE,
      '#disabled' => !$universal_device_detection_enabled,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config(self::FORM_ID);
    $config
      ->set(PushNotificationsRegistrationTokensSetting::DeleteStaleTokens->value, $form_state->getValue(PushNotificationsRegistrationTokensSetting::DeleteStaleTokens->value))
      ->set(PushNotificationsRegistrationTokensSetting::LogDeviceInfo->value, $form_state->getValue(PushNotificationsRegistrationTokensSetting::LogDeviceInfo->value))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $settings = $form_state->getValues();
    $stale_interval = $settings[PushNotificationsRegistrationTokensSetting::DeleteStaleTokens->value];

    if ($stale_interval < 0) {
      $form_state->setErrorByName('negative_stale_interval', $this->t('You must specify 0 (to disable deletion) or more than 30 days.'));
      return;
    }
    elseif ($stale_interval >= 1 && $stale_interval <= 30) {
      $form_state->setErrorByName('stale_interval_too_short', $this->t('You must specify 0 (to disable deletion) or more than 30 days.'));
      return;
    }
    elseif ($stale_interval >= 270) {
      $form_state->setErrorByName('stale_interval_too_long', $this->t('The stale interval must be less than 270 days.'));
      return;
    }
  }

}
