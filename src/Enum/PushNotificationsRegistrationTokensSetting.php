<?php

declare(strict_types=1);

namespace Drupal\push_notifications_registration_tokens\Enum;

/**
 * Settings values for Push Notifications Registration Tokens.
 */
enum PushNotificationsRegistrationTokensSetting: string {
  case LogDeviceInfo = 'log_device_info';
  case DeleteStaleTokens = 'delete_stale_tokens_after_n_days';
}
