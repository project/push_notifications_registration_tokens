<?php

declare(strict_types=1);

namespace Drupal\push_notifications_registration_tokens\Enum;

/**
 * Possible values for the push notification token type to register.
 */
enum PushNotificationTokenType: string {
  case Android = 'android';
  case Apple = 'apple';
  case Web = 'web';
}
