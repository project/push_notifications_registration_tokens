<?php

declare(strict_types=1);

namespace Drupal\push_notifications_registration_tokens;

use Drupal\push_notifications_registration_tokens\Enum\PushNotificationTokenType;
use Drupal\user\UserInterface;

/**
 * Provides an interface for handling push notification registration tokens.
 *
 * Tokens should generally not be created or deleted directly.  Instead, use
 * this interface for create, update, and delete operations.
 */
interface ProcessTokensInterface {

  /**
   * Registers or refreshes a push notification token.
   *
   * The token value is not validated with Apple/Google.
   *
   * @param string $token
   *   The push notification token to register/refresh.
   * @param \Drupal\push_notifications_registration_tokens\Enum\PushNotificationTokenType|null $token_type
   *   The type of token. Optional.
   *
   * @return bool
   *   TRUE if successful. Will throw an exception if failed.
   */
  public function registerOrRefreshToken(string $token, ?PushNotificationTokenType $token_type): bool;

  /**
   * Deletes all tokens for a specific user account.
   *
   * Used when deleting an account. Anonymizing a user's tokens makes no sense;
   * if a user cancels their account, their tokens should be removed.
   *
   * @param \Drupal\user\UserInterface $user
   *   The account to delete all tokens for.
   */
  public function deleteAllTokensForUser(UserInterface $user): void;

  /**
   * Deletes all stale tokens (which are unlikely to be used anymore).
   *
   * The administrator determines the stale interval using the config form.
   */
  public function deleteAllStaleTokens(): void;

  /**
   * Gets all tokens for a specific user account.
   *
   * Used to send notifications to all of a user's devices.
   *
   * @param \Drupal\user\UserInterface $user
   *   The account to get all tokens for.
   *
   * @returns array
   *   An array of RegistrationTokenInterface or empty if no tokens.
   */
  public function getAllTokensForUser(UserInterface $user): array;

  /**
   * Gets the token for the user's most recently used device.
   *
   * Used for sending a notification to a single device.
   *
   * @param \Drupal\user\UserInterface $user
   *   The account to get the token for.
   *
   * @returns \Drupal\push_notifications_registration_tokens\RegistrationTokenInterface|null
   *   The most recent token or NULL if no token has been registered.
   */
  public function getMostRecentTokenForUser(UserInterface $user): ?RegistrationTokenInterface;

}
